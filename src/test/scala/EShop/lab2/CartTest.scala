package EShop.lab2

import org.scalatest.FlatSpecLike

class CartTest extends FlatSpecLike {
  it should "contain one after adding one" in {
    var cart = Cart.empty
    cart = cart.addItem("ala")
    assert(cart.items.size == 1)
    assert(cart.items.head.equals("ala"))
  }

  it should "add and remove same element" in {
    var cart = Cart.empty
    cart = cart.addItem("ala")
    cart = cart.removeItem("ala")
    assert(cart.items.isEmpty)
    assert(!cart.nearlyEmpty)
  }
}