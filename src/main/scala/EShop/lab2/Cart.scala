package EShop.lab2

case class Cart(items: Seq[Any]) {
  def contains(item: Any): Boolean = items.contains(item)

  def addItem(item: Any): Cart = Cart(items :+ item)

  def removeItem(item: Any): Cart = Cart(items.filter(i => !i.equals(item)))

  def size: Int = items.size

  def nearlyEmpty: Boolean = items.size == 1

  override def toString: String = items.mkString(" ")
}

object Cart {
  def apply(item :Any): Cart = Cart(Seq(item))
  def empty: Cart = Cart(Seq.empty)
}
