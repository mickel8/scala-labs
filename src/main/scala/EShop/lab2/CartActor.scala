package EShop.lab2

import EShop.lab2.CartActor._
import akka.actor.{Actor, ActorRef, Cancellable, Props, Timers}
import akka.event.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object CartActor {
  def props = Props(new CartActor())

  sealed trait Command

  sealed trait Event

  case class AddItem(item: Any) extends Command
  case class RemoveItem(item: Any) extends Command
  case class CheckoutStarted(checkoutRef: ActorRef) extends Event
  case object ExpireCart extends Command
  case object StartCheckout extends Command
  case object CancelCheckout extends Command
  case object CloseCheckout extends Command
}

class CartActor extends Actor with Timers {

  val cartTimerDuration: FiniteDuration = 5 seconds
  private val log = Logging(context.system, this)

  def receive: Receive = empty

  def empty: Receive = {
    case itemAdded: AddItem => context become nonEmpty(Cart(itemAdded.item), scheduleTimer)
    case message: Any => context.system.deadLetters ! message
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case AddItem(item) => log.info("Adding " + item + " to "+ cart.toString())
      context become nonEmpty(cart.addItem(item), scheduleTimer)
    case RemoveItem(item) => log.info("Removing "+item+" from "+cart.toString())
      if (cart.contains(item)) {
        if (cart.nearlyEmpty) {
          context become empty
        } else {
          context become nonEmpty(cart.removeItem(item), scheduleTimer)
        }
      }
    case StartCheckout => log.info(cart.toString())
      context become inCheckout(cart)
    case ExpireCart => context become empty
    case message: Any => context.system.deadLetters ! message
  }

  def inCheckout(cart: Cart): Receive = {
    case CancelCheckout => context become nonEmpty(cart, scheduleTimer)
    case CloseCheckout => context become empty
    case message: Any => context.system.deadLetters ! message
  }

  private def scheduleTimer: Cancellable = context.system.scheduler.scheduleOnce(cartTimerDuration, self, ExpireCart)
}
