package EShop.lab2

import EShop.lab2.Checkout._
import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object Checkout {

  def props(cart: ActorRef) = Props(new Checkout())

  sealed trait Data
  sealed trait Command
  sealed trait Event

  case class SelectingDeliveryStarted(timer: Cancellable) extends Data
  case class ProcessingPaymentStarted(timer: Cancellable) extends Data
  case class SelectDeliveryMethod(method: String) extends Command
  case class SelectPayment(payment: String) extends Command
  case class PaymentStarted(payment: ActorRef) extends Event

  case object Uninitialized extends Data
  case object StartCheckout extends Command
  case object CancelCheckout extends Command
  case object ExpireCheckout extends Command
  case object ExpirePayment extends Command
  case object ReceivePayment extends Command
  case object CheckOutClosed extends Event
}

class Checkout extends Actor {

  val checkoutTimerDuration: FiniteDuration = 10 seconds
  val paymentTimerDuration: FiniteDuration = 10 seconds
  private val scheduler = context.system.scheduler
  private val log = Logging(context.system, this)

  def receive: Receive = {
    case StartCheckout => context become selectingDelivery(scheduleCheckOutTimer)
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(_) => context become selectingPaymentMethod(schedulePaymentTimer)
    case ExpireCheckout => context become cancelled
    case CancelCheckout => context become cancelled
    case message: Any => context.system.deadLetters ! message
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case ExpireCheckout => context become cancelled
    case SelectPayment(_) => context become processingPayment(schedulePaymentTimer)
    case CancelCheckout => context become cancelled
    case message: Any => context.system.deadLetters ! message
  }

  def processingPayment(timer: Cancellable): Receive = {
    case ReceivePayment => context become closed
    case CancelCheckout => context become cancelled
    case ExpirePayment => context become cancelled
  }

  def closed: Receive = {
    case message: Any => log.warning("Got " + message + " but payment was Closed")
  }

  def cancelled: Receive = {
    case message: Any => log.warning("Got " + message + " but payment was Cancelled")
  }

  private def schedulePaymentTimer: Cancellable = scheduler.scheduleOnce(paymentTimerDuration, self, ExpirePayment)

  private def scheduleCheckOutTimer: Cancellable = scheduler.scheduleOnce(checkoutTimerDuration, self, ExpireCheckout)
}
